import React from 'react'
import { Layout } from './../../componants/Layout/index';
import { Container, Form, Button, Row, Col } from 'react-bootstrap';
import { Input } from "./../../componants/UI/Input/index";
import {login } from '../../actions';
import { useDispatch } from 'react-redux';

/**
* @author
* @function Signin
**/



export const Signin = (props) => {

    const dispatch = useDispatch();

    const userLogin = (e) => { 
        
        e.preventDefault();

        const user = {
            email: 'admin@gmail.com', 
            password: '123456'
        }

        dispatch(login (user));
    }
  return(
    <Layout>
        <Container>
            <Row style= {{ marginTop: '50px' }}>
                <Col md={{ span: 6, offset: 3 }}>
                    <Form onSubmit={userLogin}>
                        <Input
                            label="Email"
                            placeholder="Email"
                            value=""
                            type="email"
                            onChange={() => {}}
                        />

                        <Input
                            label="Password"
                            placeholder="Password"
                            value=""
                            type="password"
                            onChange={() => {}}
                        />
                            
                    
                        <Button variant="primary" type="submit">
                            Sign In
                        </Button>
                    </Form>    
                </Col>
            </Row>
        
        </Container>
    </Layout>
   )

 }
 
 export default Signin