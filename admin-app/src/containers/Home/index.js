import React from 'react'
import { Layout } from './../../componants/Layout/index';

/**
* @author
* @function Home
**/

export const Home = (props) => {
  return(
   
    <Layout>
        <div style={{margin: '5rem', background: '#fff' }} className='text-center'>
            <h1>Diep</h1>
            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
        </div>
    </Layout>
    
   )

 }

 export default Home;