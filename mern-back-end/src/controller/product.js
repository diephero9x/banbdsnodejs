const Product = require("../models/product");
const multer = require("multer");
const shortid = require("shortid");
const slugify = require("slugify");

exports.createProduct = (req, res) => {
  // res.status(200).json({file: req.files, body: req.body});

  const { name, price, description, category, quantity, createdBy } =
    req.body;

    let productPictures = [];
    if(req.files.length > 0){
        productPictures = req.files.map(file => {
            return { img: file.filename}
        });
    }
  const product = new Product({
    name: req.body.name,
    slug: slugify(name),
    price,
    quantity,
    description,
    productPictures,
    category,
    createdBy: req.user._id,
  });

  product.save(((error, product) => {
    if (product) {
      res.status(201).json({ product });
    }
  }));
};
